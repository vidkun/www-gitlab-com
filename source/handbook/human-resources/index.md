---
layout: markdown_page
title: "Human Resources"
---

## GitLab, Inc. Employee Location 

### Add a New Location

1. Go to HR Passport homepage

1. Click Find

1. Click Find Location.

1. When search field appears, leave blank and click Search.

1. Click on Add location.

1. Complete location information. For a remote location, enter the location (ex. WA remote) in all fields excpet city,state and zip.

1. Click Add.

 
### Transfer Employee to Different Location

1. Go to HR Passport homepage.

1. Click Find.

1. Select find person by Name.

1. Type the name, click search.

1. From the choices, select the name.

1. On the left side of the screen, select Employment Data.

1. Select Employee Transfer.

1. Change location and fill in necessary information.

1. Select Update.

